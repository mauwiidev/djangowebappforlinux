# Django WebAppLinux

| Target   |  Status  |
|----------|:--------:|
| [WebApp](https://django-mauwiidevoqchu5wfujocm.azurewebsites.net) | [![Build Status](https://dev.azure.com/mauwiidev/djangoWebAppForLinux/_apis/build/status/deploy%20bicep?branchName=main)](https://dev.azure.com/mauwiidev/djangoWebAppForLinux/_build/latest?definitionId=31&branchName=main) |

## Intro

I created this Project for training purpose and to play around a little bit with DevOps and Azure Pipelines.

## original readme

This is the sample Django application for the Azure Quickstart [Deploy a Python (Django or Flask) web app to Azure App Service](https://docs.microsoft.com/en-us/azure/app-service/quickstart-python).  For instructions on how to create the Azure resources and deploy the application to Azure, refer to the Quickstart article.

A Flask sample application is also available for the article at [https://github.com/Azure-Samples/msdocs-python-flask-webapp-quickstart](https://github.com/Azure-Samples/msdocs-python-flask-webapp-quickstart).

If you need an Azure account, you can [create one for free](https://azure.microsoft.com/en-us/free/).
